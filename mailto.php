<?php
session_start();

$mailto = "info@ml.unalus.com";  // 宛先メールアドレス
$subject = "ホームページにお問い合わせがありました"; // 件名
$subject .= "【" . $_SESSION["contactKind"] . "】";
$content = "ホームページにお問い合わせがありました。\n";
$content .= "問い合わせ内容は以下の通りです。\n\n";
$content .= "■問い合わせ種別\n" . $_SESSION["contactKind"] . "\n\n";
$content .= "■お名前\n" . $_SESSION["name"] . "\n\n";
$content .= "■メールアドレス\n" . $_SESSION["mail"] . "\n\n";
$content .= "■電話番号\n" . $_SESSION["phone"] . "\n\n";
$content .= "■会社名\n" . $_SESSION["companyName"] . "\n\n";
$content .= "■お問い合わせ内容\n" . $_SESSION["inquiry"];

$headers = <<<HEAD
Content-Type: text/plain;charset=ISO-2022-JP
HEAD;

$is_success = mb_send_mail($mailto, $subject, $content, $headers);

if(!$is_success) {
  die('メールの送信に失敗しました。');
}

?>

<!DOCTYPE html>
<html lang="ja" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="ウナルステクノロジー株式会社の企業サイトです。システム開発受託、SE支援・開発支援サービス、コンサルタント、WEBサイトの開発・運営、Android/iPhoneアプリ企画/開発などをおこなっています。" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>お問い合わせ完了｜ウナルステクノロジー株式会社</title>
    <meta property="og:title" content="ウナルステクノロジー株式会社" />
    <meta property="og:type" content="website" />
    <meta property="og:description" content="ウナルステクノロジー株式会社の企業サイトです。システム開発受託、SE支援・開発支援サービス、コンサルタント、WEBサイトの開発・運営、Android/iPhoneアプリ企画/開発などをおこなっています。" />
    <meta property="og:url" content="http://unalus.com/" />
    <meta property="og:site_name" content="ウナルステクノロジー株式会社" />
    <meta property="og:image" content="http://unalus.com/images/unalus.com.png" />
    <link rel="icon" href="logo/favicon.ico">
    <link rel="canonical" href="http://unalus.com/">
    <script type="text/javascript" src="./js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="./js/script.js"></script>
    <link rel="stylesheet" type="text/css" href="./css/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/yakuhanjp@3.2.0/dist/css/yakuhanjp.min.css">
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
</head>
<body>
    <nav class="sp_navigation">
        <div class="nav_header cf">
            <div class="nav_header_logo">
                <a href="http://unalus.com/"><img src="logo/ut_logo11.png"></a>
            </div>
            <div class="close_btn"></div>
        </div>
        <div class="nav_block">
            <ul>
                <li><a href="#news">ニュース</a></li>
                <li><a href="#bussiness">事業内容</a></li>
                <li><a href="#company">会社情報</a></li>
                <li><a href="http://unalus.com/contact.html">お問い合わせ</a></li>
                <li><a href="http://unalus.com/wp/">ブログ</a></li>
                <li><a href="http://wiki.unalus.com/index.php?plugin=loginform&page=UnalusTechnology&url_after_login=http%3A%2F%2Fwiki.unalus.com%2Findex.php%3F">ウナルスwiki</a></li>
            </ul>
        </div>
    </nav>
    <div class="wrapper">
        <header class="cf" id="site_header">
            <h1 class="company_name cf">
                <a href="http://unalus.com/">
                    <div class="logo">
                        <img src="logo/ut_logo11.png" class="first"><img src="images/unalus.png" class="second">
                    </div>
                </a>
            </h1>
            <ul class="header_list cf">
                <li><a href="#news">ニュース</a></li>
                <li><a href="#bussiness">事業内容</a></li>
                <li><a href="#company">会社情報</a></li>
                <li class="current"><a href="http://unalus.com/contact.html">お問い合わせ</a></li>
                <li><a href="http://unalus.com/wp/">ブログ</a></li>
                <li><a href="http://wiki.unalus.com/index.php?plugin=loginform&page=UnalusTechnology&url_after_login=http%3A%2F%2Fwiki.unalus.com%2Findex.php%3F">ウナルスwiki</a></li>
            </ul>
            <div class="sp_navigation_btn">
                <span class="lines">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </div>
        </header>
        <div class="contact_wrapper" id="contact">
            <div class="contact_title">
                <h2>お問い合わせ</h2>
            </div>
            <div class="cf">
                <div class="contact_left">
                    <div class="contact_flow" id="flow">
                        <div class="txt">
                            <span>入力</span>
                            <span>&nbsp;&nbsp;―&nbsp;&nbsp;</span>
                            <span>確認</span>
                            <span>&nbsp;&nbsp;―&nbsp;&nbsp;</span>
                            <span class="current">完了</span>
                        </div>
                    </div>
                </div>
                <div class="contact_right">
                    <p class="thanks">お問い合わせいただきありがとうございます。</p>
                </div>
            </div>
        </div>
        <div class="inquiry_wrapper" id="qa">
            <a href="http://unalus.com/contact.html">
                <div class="inquiry_title">
                    <h2>お問い合わせ</h2>
                    <p>お問い合わせはこちらから</p>
                </div>
            </a>
        </div>
    </div>
    <footer>
        <div class="copyright">&copy;Copyright Unalus Technology Inc. All rights reserved.</div>
    </footer>
</body>
</html>