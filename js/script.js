/*global window */

jQuery(function($){

    // スクロールでヘッダーを上部固定のスタイルに
    var headerPosi = 30;
    $(window).scroll(function(){
        if($(this).scrollTop() > headerPosi){
            $('#site_header').addClass('fixed');
        } else {
            $('#site_header').removeClass('fixed');
        }
    });

    // ページ内リンクのずれ調整用
    var headerHight = 77; //ヘッダの高さ
    $('a[href^=#]').click(function(){
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top-headerHight; //ヘッダの高さ分位置をずらす
        $("html, body").animate({scrollTop:position}, 550, "swing"); //この数値は移動スピード
           return false;
    });

    // ハンバーガーメニュークリックでメニュー展開
    $('.sp_navigation_btn').on('click', function(){
        var posi = $(window).scrollTop();
        $('.wrapper').css({
            top: -1 * posi
        });
        $('.sp_navigation').addClass('is_open');
    });

    // 閉じるボタンクリックでメニューしまう
    $('.close_btn').click(function(){
        $('.sp_navigation').removeClass('is_open');
    });

    // 左からフェードイン
    $(window).on('scroll', function(){
        var box = $('.box');
        box.each(function(){
            var boxOffset = $(this).offset().top;
            var scrollPos = $(window).scrollTop();
            var wh = $(window).height();

            if(scrollPos > boxOffset - wh + (wh / 2)){
                $(this).addClass('isAnimate');
            }
        });
    });
    
    // お問い合わせページの入力チェック（テキストボックス_フォーカス）
    $('.contact_wrapper input[type="text"]').blur(function() {
        // テキストボックスが空だった場合
        if ($(this).val() == ''){
            // 警告文表示
            $(this).next().addClass('is_show error');
        } else {
            // 警告文隠す
            $(this).next().removeClass('is_show error');
        }
    });

    // お問い合わせの入力チェック（テキストエリア_フォーカス）
    $('.contact_wrapper textarea').blur(function() {
        // テキストボックスが空だった場合
        if ($(this).val() == ''){
            // 警告文表示
            $(this).next().addClass('is_show error');
        } else {
            // 警告文隠す
            $(this).next().removeClass('is_show error');
        }
    });

    // お問い合わせフォームでのenterキーでのsubmit無効化
    document.onkeypress = enter;
    function enter(){
        if( window.event.keyCode == 13 ){
            return false;
        }
    }
    
    // お問い合わせの入力チェック
    $('input:submit[id="go_confirm"]').click(function(){
        CheckInquiry();
        var count = $('.error').length;
        if(count !== 0) {
            return false;
        }
    });
    
    function CheckInquiry() {
        $(".caution").empty().removeClass("error");
        $("#contactKind_error").empty().removeClass("error");
        $("#name_error").empty().removeClass("error");
        $("#mail_error").empty().removeClass("error");
        $("#phone_error").empty().removeClass("error");
        $("#companyName_error").empty().removeClass("error");
        $("#inquiry_error").empty().removeClass("error");
        
        // 問い合わせ種別
        if($("input[name='contactKind']:checked").length == 0){
            $("#contactKind_error").text("問い合わせ種別が選択されていません").addClass("error");
        }
        // お名前
        if($("#name").val() == "") {
            $("#name_error").text("お名前が入力されていません").addClass("error");
        }
        // メールアドレス
        if($("#mail").val() == ""){
            $("#mail_error").text("メールアドレスが入力されていません").addClass("error");
        } else if(!$("#mail").val().match(/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/)){
            $('#mail_error').text("正しいメールアドレスを入力してください").addClass("error");
        } else if($("#mail").val().length > 255){
            $("mail_error").text("正しいメールアドレスを入力してください").addClass("error");
        }
        // 電話番号
        if($("#phone").val() == ""){
            $("#phone_error").text("電話番号が入力されていません").addClass("error");
        } else if((!$("#phone").val().match(/^[0-9]+$/)) || (12 > $("#phone").val().length > 9 )){
            $("#phone_error").text("正しい電話番号を入力してください").addClass("error");
        }
        // 会社名
        if($("#companyName").val() == ""){
            $("#companyName_error").text("会社名が入力されていません").addClass("error");
        }
        // お問い合わせ内容
        if($("#inquiry").val() == ""){
            $("#inquiry_error").text("お問い合わせ内容が入力されていません").addClass("error");
        }
        else if($("#inquiry").val().match(/^[\r\n\t]*$/)){
            $("#inquiry_error").text("お問い合わせ内容が入力されていません").addClass("error");
        }   
        
        // エラーの際の処理
        if($(".error").length !== 0) {
            $("html,body").animate({
                scrollTop: $(".contact_head").offset().top-headerHight
            }, "swing");
            return false;
        } 
        else {
            $('input:submit[id="go_confirm"]').off('submit');
            $('input:submit[id="go_confirm"]').submit();
        }
        
        return false;
        
    }        

});